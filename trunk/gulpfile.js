/*

* Pulgin *
gulp				    : gulp
gulp-ejs 			    : ejsからhtmlに書き出し
gulp-sass 			    : sassからcssに書き出し
gulp-imagemin 		    : 画像圧縮
imagemin-pngquant       : 圧縮率を高めるのにプラグインを入れる png
imagemin-pngquant       : 圧縮率を高めるのにプラグインを入れる jpg
gulp-plumber 		    : エラーが出てもタスクを落とさないように制御
gulp-cached 			: 変更されたファイルだけを処理
gulp-changed 			: 変更されたファイル(画像)だけを処理
browser-sync 		    : ブラウザ起動

*/

var gulp = require("gulp");
var ejs = require("gulp-ejs");
var sass = require("gulp-sass");
var imagemin = require("gulp-imagemin");
var pngquant = require('imagemin-pngquant');
var mozjpeg = require('imagemin-mozjpeg');
//var cache = require('gulp-cached');
var changed  = require('gulp-changed');
var browser = require("browser-sync");
var plumber = require("gulp-plumber");

// ブラウザ起動と自動リロード
gulp.task("server", function() {
    browser({
        server: {
            baseDir: "./build"
        }
    });
});

// html : EJS から HTML生成
gulp.task("ejs", function() {
    gulp.src(
        ["src/**/*.ejs",'!' + "src/**/_*.ejs"]
    )
    //.pipe(cache('ejs'))
    .pipe(plumber())
    .pipe(ejs({}, {}, { ext: '.html' }))
    .pipe(gulp.dest("./build/"))
    .pipe(browser.reload({stream:true}))
});

// css : sass から css 生成
gulp.task("sass", function() {
    gulp.src("src/**/*.scss")
    //.pipe(cache('sass'))
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest("./build/"))
    .pipe(browser.reload({stream:true}))
});

// img : 画像の圧縮
gulp.task('img', function(){
    gulp.src("src/**/*.{png,jpg,gif,svg}")
    .pipe(changed("./build/"))
    .pipe(imagemin([
        pngquant({
        quality: '65-80',  // 画質
        speed: 1,  // 最低のスピード
        floyd: 0,  // ディザリングなし
        }),
        mozjpeg({
        quality: 85, // 画質
        progressive: true
        }),
        imagemin.svgo(),
        imagemin.optipng(),
        imagemin.gifsicle()
    ]))
    .pipe(gulp.dest("./build/"))
    .pipe(browser.reload({stream:true}))
});

// ウォッチタスク
gulp.task("default",['server'],function() {
    gulp.watch(["src/**/*.ejs"],["ejs"]);
    gulp.watch(["src/**/*.scss"],["sass"]);
    gulp.watch(["src/**/*.{png,jpg,gif,svg}"], ['img']);
});